﻿namespace BanksySan.Learning.WebLoadTestingWebServer
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Nancy;
    using Nancy.Hosting.Self;
    using Newtonsoft.Json;

    class Program
    {
        static int Main()
        {
            const string PORT = "9000";

            var uri = $"http://localhost:{PORT}";

            using (var server = new NancyHost(new Uri(uri)))
            {
                try
                {
                    server.Start();
                }
                catch (AutomaticUrlReservationCreationFailureException)
                {
                    Console.WriteLine($"Unable to create port at {PORT}.");
                    Console.WriteLine(
                        "You'll either need to run this as admin or run the following at an admin command prompt:");
                    Console.WriteLine($"    netsh http add urlacl url=\"http://+:{PORT}/\" user=\"Everyone\"");
                    return 1;
                }

                Console.WindowWidth = 50;
                Console.BufferWidth = 50;
                Console.Title = uri + " | Web Load Test Web Server";
                Console.WriteLine("Listening on {0}", uri);
                Console.WriteLine("Press enter to stop.");
                Console.Read();
            }

            return 0;
        }
    }

    public class RootModule : NancyModule
    {
        private static readonly IDictionary<int, string> DETAILS = new ConcurrentDictionary<int, string>();

        public RootModule()
        {
            Get["/"] = p =>
                       {
                           var responseBody =
                               JsonConvert.SerializeObject(DETAILS.Select(x => new { Id = x.Key, Message = x.Value }));
                           var response = new Response();
                           var jsonBytes = Encoding.UTF8.GetBytes(responseBody);

                           response.Headers.Add("Content-Type", "text/json");
                           response.Contents = stream => stream.Write(jsonBytes, 0, jsonBytes.Length);
                           return response;
                       };

            Get["/{id}"] = p =>
                           {
                               if (DETAILS.ContainsKey(p.Id))
                               {
                                   var newId = p.Id;
                                   var response = new Response();

                                   var jsonBytes = Encoding.UTF8.GetBytes($"{{\"message\": \"{DETAILS[newId]}\" }}");

                                   response.Headers.Add("Content-Type", "text/json");
                                   response.Contents = stream => stream.Write(jsonBytes, 0, jsonBytes.Length);
                                   return response;
                               }
                               else
                               {
                                   var response = new Response();

                                   response.Headers.Add("Content-Type", "text/json");
                                   response.StatusCode = HttpStatusCode.NotFound;
                                   return response;
                               }
                           };

            Post["/{message}"] = p =>
                                 {
                                     var message = p.Message;
                                     var newId = DETAILS.Keys.Count + 1;
                                     DETAILS.Add(newId, message);

                                     var response = new Response();

                                     var jsonBytes = Encoding.UTF8.GetBytes($"{{\"Id\": {newId} }}");

                                     response.Headers.Add("Content-Type", "text/json");
                                     response.Contents = stream => stream.Write(jsonBytes, 0, jsonBytes.Length);
                                     return response;
                                 };
        }
    }
}